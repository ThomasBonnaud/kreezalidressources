jourSemaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"];

function isMobileDevice() {
    return (navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/iPad/i) ||
        navigator.userAgent.match(/iPod/i) ||
        navigator.userAgent.match(/BlackBerry/i) ||
        navigator.userAgent.match(/Windows Phone/i)
    )
}

function makeGift() {
    document.querySelector("#isAgift").setAttribute("value", "1")
    document.querySelector("#form_booking_create").submit()
}

function showAdditonalContent(elementSelected, voirPlus, additionalFilters, bool) {
    text = document.createElement("text");
    text.innerHTML = "voir moins -";
    text.style = "font-weight: bold";

    for (i = 0, j = 0; i < voirPlus.length; i++, j++) {
        if (bool === false && voirPlus[i].id === elementSelected.id) {
            i++;
            voirPlus[i].appendChild(additionalFilters[j]);
            voirPlus[i].appendChild(text);
            voirPlus[i].children[1].addEventListener("click", function() { showAdditonalContent(elementSelected, voirPlus, additionalFilters, true) });
            voirPlus[i].children[0].setAttribute("class", "list-unstyled");
            voirPlus[i].children[1].setAttribute("class", "collapsible");
            voirPlus[i - 1].style.display = "none";
            voirPlus[i].style.display = "block";
        } else if (bool === true && voirPlus[i + 1].children[0]) {
            i++;
            voirPlus[i].style.display = "none";
            voirPlus[i - 1].style.display = "block";
            voirPlus[i].removeChild(voirPlus[i].firstChild);
            voirPlus[i].removeChild(voirPlus[i].firstChild);
        } else {
            i++;
        }
    }
    return voirPlus;
}


function handleExpandbleListing() {
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }
}

if (document.querySelector(".listing-attribut-lieu_proposes div") != null) {
    document.querySelector(".listing-attribut-lieu_proposes div").remove();
}

//trick pour la newsletter de omar bolivie à supprimer par la suite
if (window.location.href == "https://neosilver.fr/listing/djc8yn2zphj-visite-virtuelle-en-direct-de-sucre-capitale-de-la-bolivie?utm_source=newsletter&utm_medium=email&utm_campaign=sur_la_route_on_a_decide_de_vous_faire_voyager&utm_term=2020-06-30") {
    window.location = "https://neosilver.fr/listing/krztr4icico-visite-virtuelle-en-direct-de-sucre-capitale-de-la-bolivie";
}
if (window.location.search.search("lGift=1") >= 0) {
    let booking = document.querySelector(".booking_summary_box")
    let list = document.createElement("ul")
    list.style = "padding-left : 10px"
    let subList = document.createElement("li")
    subList.appendChild(document.createTextNode("Je reçois "))
    let strong = document.createElement("strong")
    strong.appendChild(document.createTextNode("un mail de confirmation"))
    subList.appendChild(strong)
    subList.appendChild(document.createTextNode(" de la réservation"))
    list.appendChild(subList)
    subList = document.createElement("li")
    subList.appendChild(document.createTextNode("J'imprime ou je transfère le mail "))
    strong = document.createElement("strong")
    strong.appendChild(document.createTextNode("à mon bénéficiaire."))
    subList.appendChild(strong)
    list.appendChild(subList)
    booking.insertBefore(list, booking.childNodes[6])
    booking.insertBefore(document.createElement('hr'), booking.childNodes[7])
}

if (window.location.search.search("=38755") >= 0 || window.location.search.search("=38754") >= 0 || window.location.search.search("=38753") >= 0) {
    let booking = document.querySelector(".booking_summary_box")
    let list = document.createElement("ul")
    list.style = "padding-left : 10px"
    let subList = document.createElement("li")
    subList.appendChild(document.createTextNode("Je reçois "))
    let strong = document.createElement("strong")
    strong.appendChild(document.createTextNode("un mail de confirmation"))
    subList.appendChild(strong)
    subList.appendChild(document.createTextNode(" avec la carte cadeau."))
    list.appendChild(subList)
    subList = document.createElement("li")
    subList.appendChild(document.createTextNode("J'imprime ou je transfère le mail "))
    strong = document.createElement("strong")
    strong.appendChild(document.createTextNode("à mon bénéficiaire."))
    subList.appendChild(strong)
    list.appendChild(subList)
    subList = document.createElement("li")
    subList.appendChild(document.createTextNode("Mon bénéficiaire contacte l'équipe Neosilver par email ou téléphone pour "))
    strong = document.createElement("strong")
    strong.appendChild(document.createTextNode("choisir son ou ses activités."))
    subList.appendChild(strong)
    list.appendChild(subList)
    booking.insertBefore(list, booking.childNodes[6])
    booking.insertBefore(document.createElement('hr'), booking.childNodes[7])
}

if (window.location.pathname !== "/listing/category/cartes-cadeaux-2") {
    let carteCadeau = document.querySelector("#listing-thumbnail-38115")
    if (carteCadeau) {
        carteCadeau.style = "display : none"
    }
    carteCadeau = document.querySelector("#listing-thumbnail-37524")
    if (carteCadeau) {
        carteCadeau.style = "display : none"
    }
    carteCadeau = document.querySelector("#listing-thumbnail-38753")
    if (carteCadeau) {
        carteCadeau.style = "display : none"
    }
    carteCadeau = document.querySelector("#listing-thumbnail-38754")
    if (carteCadeau) {
        carteCadeau.style = "display : none"
    }
    carteCadeau = document.querySelector("#listing-thumbnail-38755")
    if (carteCadeau) {
        carteCadeau.style = "display : none"
    }
}

if (window.location.pathname === "/listing/category/cartes-cadeaux-2") {
    let sideFilter = document.querySelector('.filter_sidebar')
    sideFilter.style = "display : none"
}

let listingFilter = document.querySelector("#form_listing_filter")
if (listingFilter) {
    let catID = document.querySelector("#cat-id")
    if (catID) {
        catID.lastElementChild.style = "display:none"
        catID = catID.parentNode
        catID.style = "border-radius: 12px; overflow: hidden;"

        listingFilter.insertBefore(catID, listingFilter.firstElementChild)
        catID.removeAttribute("required")

    }
    let gmapFilter = document.querySelector("#gmap-input-area")
    if (gmapFilter) {
        gmapFilter.removeAttribute("required")
    }
    let dateFilter = document.querySelector("#search-date-calendar")
    if (dateFilter) {
        dateFilter.removeAttribute("required")
        dateFilter.placeholder = "Pas de date précise";
        dateFilter.value = "Pas de date précise";
        dateFilter.type = "search";
        dateFilter.data("DateTimePicker").date("test")
    }

}

var formPassword = document.querySelector(".form-group.password.required")
if (formPassword) {
    let inputPassword = document.querySelector("#password")
    if (inputPassword) {
        let labelCheckbox = document.createElement('label')
        labelCheckbox.className = "label-checkbox-password"
        labelCheckbox.style = "text-align : right ; display : block; font-weight : 400"
        let inputCheckBox = document.createElement('input')
        inputCheckBox.className = "checkbox-password"
        inputCheckBox.type = "checkbox"
        inputCheckBox.setAttribute("onclick", "HideShowPassword()")
        labelCheckbox.appendChild(inputCheckBox)
        labelCheckbox.appendChild(document.createTextNode(" Afficher / Masquer"))
        inputPassword.parentNode.appendChild(labelCheckbox)
    }
    let inputPasswordCurrent = document.querySelector("#current-password")
    if (inputPasswordCurrent) {
        let labelCheckboxCurrent = document.createElement('label')
        labelCheckboxCurrent.className = "label-checkbox-password"
        labelCheckboxCurrent.style = "text-align : right ; display : block; font-weight : 400"
        let inputCheckBoxCurrent = document.createElement('input')
        inputCheckBoxCurrent.className = "checkbox-password"
        inputCheckBoxCurrent.type = "checkbox"
        inputCheckBoxCurrent.setAttribute("onclick", "HideShowPasswordCurrent()")
        labelCheckboxCurrent.appendChild(inputCheckBoxCurrent)
        labelCheckboxCurrent.appendChild(document.createTextNode(" Afficher / Masquer"))
        inputPasswordCurrent.parentNode.appendChild(labelCheckboxCurrent)
    }
    let inputPasswordConfirm = document.querySelector("#password-confirm")
    if (inputPasswordConfirm) {
        let labelCheckboxConfirm = document.createElement('label')
        labelCheckboxConfirm.className = "label-checkbox-password"
        labelCheckboxConfirm.style = "text-align : right ; display : block; font-weight : 400"
        let inputCheckBoxConfirm = document.createElement('input')
        inputCheckBoxConfirm.className = "checkbox-password"
        inputCheckBoxConfirm.type = "checkbox"
        inputCheckBoxConfirm.setAttribute("onclick", "HideShowPasswordConfirm()")
        labelCheckboxConfirm.appendChild(inputCheckBoxConfirm)
        labelCheckboxConfirm.appendChild(document.createTextNode(" Afficher / Masquer "))
        inputPasswordConfirm.parentNode.appendChild(labelCheckboxConfirm)
    }
}

function HideShowPassword() {
    let password = document.querySelector("#password")
    if (password.type === "password") {
        password.type = "text";
    } else {
        password.type = "password";
    }
}

function HideShowPasswordConfirm() {
    let password = document.querySelector("#password-confirm")
    if (password.type === "password") {
        password.type = "text";
    } else {
        password.type = "password";
    }
}

function HideShowPasswordCurrent() {
    let password = document.querySelector("#current-password")
    if (password.type === "password") {
        password.type = "text";
    } else {
        password.type = "password";
    }
}

var nbAccount = document.querySelector('#account-number')
if (nbAccount) {
    var divNb = nbAccount.parentNode.childNodes[0].childNodes[0]
    divNb.data = "IBAN"
}

var userNavBar = document.querySelector('.user_account_menu')
if (userNavBar) {
    userNavBar.setAttribute("onmouseover", "DropDown(this)")
    userNavBar.setAttribute("onmouseout", "DropUp(this)")
}

function DropDown(x) {
    x.children[1].style = "display : block"
}

function DropUp(x) {
    x.children[1].style = "display : none"
}

var querySearch = document.querySelector('#query')
if (querySearch) {
    query.placeholder = "Quelle activité ?"
}

var queryLocation = document.querySelector('#gmap-input-area')
if (queryLocation) {
    queryLocation.placeholder = "où ? (code postal, rue,...)"
}

var alert = document.querySelector('.vendor_kyc_alert')
if (alert) {
    var header = document.querySelector('header')
    header.appendChild(alert)
    var mainContent = document.querySelector('.main-content')
    mainContent.style = "margin-top : 0px !important; top : 0 !important;"
}

var listing = Kr.Listing;

if (window.location.pathname == "/register/") {
    var sectionInscription = document.querySelector('.register .wh-part .wh-part-head')
    var register = document.querySelector('.register .register')
    register.className = "col-md-8 col-md-offset-2 col-xs-12 register"
    var proSection = document.createElement('p')
    proSection.style = "text-align : center"
    var proLink = document.createElement('a')
    proLink.href = "https://neosilver.fr/register/professionnel"
    proLink.appendChild(document.createTextNode("Cliquez-ici."))
    proSection.appendChild(document.createTextNode("Vous souhaitez proposer des activités en tant que professionnel(s). "))
    proSection.appendChild(proLink)
    var userSection = document.createElement('p')
    userSection.style = "text-align : center"
    userSection.appendChild(document.createTextNode("Si non vous pouvez poursuivre votre inscription."))
    sectionInscription.appendChild(proSection)
    sectionInscription.appendChild(userSection)
}

if (window.location.pathname == "/register") {
    var sectionInscription = document.querySelector('.register .wh-part .wh-part-head')
    var register = document.querySelector('.register .register')
    register.className = "col-md-8 col-md-offset-2 col-xs-12 register"
    var proSection = document.createElement('p')
    proSection.style = "text-align : center"
    var proLink = document.createElement('a')
    proLink.href = "https://neosilver.fr/register/professionnel"
    proLink.appendChild(document.createTextNode("Cliquez-ici."))
    proSection.appendChild(document.createTextNode("Vous souhaitez proposer des activités en tant que professionnel(s). "))
    proSection.appendChild(proLink)
    var userSection = document.createElement('p')
    userSection.style = "text-align : center"
    userSection.appendChild(document.createTextNode("Si non vous pouvez poursuivre votre inscription."))
    sectionInscription.appendChild(proSection)
    sectionInscription.appendChild(userSection)
}

if (window.location.pathname == "/faq") {
    var navBarFAQ = document.querySelector('.navbar-collapse.in')
    navBarFAQ.className = "navbar-collapse"
}

if (window.location.pathname.search("psp/account/create") >= 0) {
    var addrInput = document.querySelector('#address-line1')
    addrInput.required = "required";
    var postalInput = document.querySelector('#postal-code')
    postalInput.required = "required";
    var cityInput = document.querySelector('#city')
    cityInput.required = "required";
}

if (window.location.pathname == "/register/professionnel") {
    var sectionProInscription = document.querySelector('.register .wh-part .wh-part-head h1')
    sectionProInscription.appendChild(document.createTextNode(" Professionnel "))
    var register = document.querySelector('.register .register')
    register.className = "col-md-8 col-md-offset-2 col-xs-12 register registerPro"
    var year = document.querySelector('.date .year option')
    year.setAttribute("selected", "")

}

if (window.location.pathname == "/cartes-cadeaux-2") {
    var header = document.querySelector('.page-header')
    header.style = "display : none"
    var section = document.querySelector('.cadeau')
    section.style = "padding: 50px 0 50px;"
    var page = document.querySelector('.main-content')
    page.style = "background-color : #f9f9f9; padding-bottom : 0px !important"
    var page2 = document.querySelector('.cms-page')
    page2.style = "padding-bottom : 0px !important"
}

// if (window.location.pathname == "/cartes-cadeaux") {
//     var header = document.querySelector(".page-header");
//     header.style = "display : none;";
//     var page = document.querySelector(".main-content");
//     page.style = "background-color : #f9f9f9; padding-bottom : 0px !important;";

//     let containerDiv = document.querySelector(".cms-page");
//     let firstBanner = document.createElement("div");
//     let secondBanner = document.createElement("div");
//     let thirdBanner = document.createElement("div");

//     containerDiv.style = "background-color: white; padding-bottom : 0px !important";
//     firstBanner.id = "firstBanner";
//     secondBanner.id = "secondBanner";
//     thirdBanner.id = "thirdBanner";

//     containerDiv.insertBefore(firstBanner, containerDiv.firstChild);
//     containerDiv.insertBefore(secondBanner, containerDiv.firstChild);
//     containerDiv.insertBefore(thirdBanner, containerDiv.firstChild);
// }

if (window.location.pathname == "/newsletter-club-neosilver") {
    var header = document.querySelector('.page-header')
    header.style = "display : none"
    var page = document.querySelector('.main-content')
    page.style = "background-color : #f9f9f9; padding-bottom : 0px !important"
    var page2 = document.querySelector('.cms-page')
    page2.style = "padding-bottom : 0px !important"
}

if (window.location.pathname == "/qui-sommes-nous") {
    var header = document.querySelector('.page-header')
    header.style = "display : none"
    var page = document.querySelector('.main-content')
    page.style = "background-color : #f9f9f9; padding-bottom : 0px !important"
    var page2 = document.querySelector('.cms-page')
    page2.style = "padding-bottom : 0px !important"
}

if (window.location.pathname == "/ils-parlent-de-nous") {
    var header = document.querySelector('.page-header')
    header.style = "display : none"
    var page = document.querySelector('.main-content')
    page.style = "background-color : #f9f9f9; padding-bottom : 0px !important"
    var page2 = document.querySelector('.cms-page')
    page2.style = "padding-bottom : 0px !important"
}

if (window.location.pathname == "/nos-partenaires") {
    var header = document.querySelector('.page-header')
    header.style = "display : none"
    var page = document.querySelector('.main-content')
    page.style = "background-color : #f9f9f9; padding-bottom : 0px !important"
    var page2 = document.querySelector('.cms-page')
    page2.style = "padding-bottom : 0px !important"
}

if (window.location.pathname == "/comment-ca-marche") {
    var header = document.querySelector('.page-header')
    header.style = "display : none"
    var page = document.querySelector('.main-content')
    page.style = "background-color : #f9f9f9; padding-bottom : 0px !important"
    var page2 = document.querySelector('.cms-page')
    page2.style = "padding-bottom : 0px !important"
}

if (window.location.pathname.search("/listings") != -1 || window.location.pathname.search("/listing/category/") != -1 || window.location.pathname.search("/listing/search") != -1) {
    let FormFilter = document.querySelector('#listingFiltersForm')
    let momentJ = null
    if (document.querySelector('#moment-de-la-journee-176710')) {
        momentJ = document.querySelector('#moment-de-la-journee-176710').parentNode.parentNode.parentNode.parentNode.parentNode
    } else if (document.querySelector('#moment-de-la-journee-176711')) {
        momentJ = document.querySelector('#moment-de-la-journee-176711').parentNode.parentNode.parentNode.parentNode.parentNode
    } else if (document.querySelector('#moment-de-la-journee-176712')) {
        momentJ = document.querySelector('#moment-de-la-journee-176712').parentNode.parentNode.parentNode.parentNode.parentNode
    } else if (document.querySelector('#moment-de-la-journee-176713')) {
        momentJ = document.querySelector('#moment-de-la-journee-176713').parentNode.parentNode.parentNode.parentNode.parentNode
    }
    if (momentJ) {
        let momentTitle = momentJ.firstElementChild
        momentJ.insertBefore(momentTitle, momentJ.firstElementChild)
        FormFilter.insertBefore(momentJ, FormFilter.firstElementChild)
    }
    let TypeOffer = null
    if (document.querySelector('#type-d-offre-176632')) {
        TypeOffer = document.querySelector('#type-d-offre-176632').parentNode.parentNode.parentNode.parentNode.parentNode
    } else if (document.querySelector('#type-d-offre-176633')) {
        TypeOffer = document.querySelector('#type-d-offre-176632').parentNode.parentNode.parentNode.parentNode.parentNode
    } else if (document.querySelector('#type-d-offre-176634')) {
        TypeOffer = document.querySelector('#type-d-offre-176634').parentNode.parentNode.parentNode.parentNode.parentNode
    }
    if (TypeOffer) {
        let TypeOfferTitle = TypeOffer.firstElementChild
        TypeOffer.insertBefore(TypeOfferTitle, TypeOffer.firstElementChild)
        FormFilter.insertBefore(TypeOffer, FormFilter.firstElementChild)
    }

    if (window.location.pathname.search("/listing/category/") != -1) {
        if (document.querySelector('.price_filter_side') != null) {
            let categoryFilter = document.querySelector('.price_filter_side').nextElementSibling.nextElementSibling
            categoryFilter.insertBefore(categoryFilter.previousElementSibling, categoryFilter.firstElementChild)
            categoryFilter.style = "display: none"
        } else if (document.querySelector('.price_filter_side') == null) {
            let categoryFilter = document.querySelector('filter-niveau_requis_2').nextElementSibling
            categoryFilter.insertBefore(categoryFilter.previousElementSibling, categoryFilter.firstElementChild)
            categoryFilter.style = "display: none"
        }
    }
    let listFilters = document.getElementById("listingFiltersForm")
    let text = document.createElement("text");
    text.innerHTML = "voir moins -";
    let nbLiFilters;
    let voirPlus = [];
    let additionalFilters = [];
    let bool = false;
    let nbExpandable = 0;
    nbAdditionalContent = 0;

    for (i = 0; i < listFilters.childElementCount; i++) {
        let nbLiFilters = 0;

        for (y = 0;
            (y < listFilters.children[i].childElementCount * 2); y++) {
            if (!listFilters.children[i].childNodes[y]) {
                break;
            } else if (listFilters.children[i].childNodes[y].nodeName == "LI") {
                nbLiFilters++;
            }
            if (nbLiFilters > 4) {
                additionalFilters[nbAdditionalContent] = document.createElement("ul");
                additionalFilters.className = "list-unstyled";
                additionalFilters[nbAdditionalContent].style.marginTop = "-10px";
                voirPlus[nbExpandable] = document.createElement("text");
                voirPlus[nbExpandable].id = nbExpandable + "filter";
                voirPlus[nbExpandable].innerHTML = "voir plus +";
                voirPlus[nbExpandable].className = "collapsible";
                voirPlus[nbExpandable].style = "font-weight: bold";
                voirPlus[nbExpandable].addEventListener("click", function() {
                    (showAdditonalContent(document.getElementById(this.id), voirPlus, additionalFilters, bool))
                });
                voirPlus[nbExpandable + 1] = document.createElement("div");
                voirPlus[nbExpandable + 1].className = "content";
                while (listFilters.children[i].childNodes[y] != null &&
                    listFilters.children[i].childNodes[y].nodeName == "LI") {
                    additionalFilters[nbAdditionalContent].appendChild(listFilters.children[i].childNodes[y]);
                    additionalFilters[nbAdditionalContent].style.marginLeft = "-16px";
                    y++;
                }
                listFilters.children[i].appendChild(voirPlus[nbExpandable]);
                listFilters.children[i].appendChild(voirPlus[nbExpandable + 1]);
                nbAdditionalContent++;
                nbExpandable = nbExpandable + 2;
                break;
            }
        }
    }

    let listSearchFilters = document.querySelector('.search-filters')

    for (i = 0; i < listSearchFilters.childElementCount; i++) {
        nbLiFilters = 0;
        if (!(listSearchFilters.children[i].children[0])) {} else {
            for (y = 0;
                (y < listSearchFilters.children[i].children[1].childElementCount * 2); y++) {
                if (listSearchFilters.children[i].children[1].childNodes[y].nodeName == "LI") {
                    nbLiFilters++;
                }
                if (nbLiFilters > 4) {
                    additionalFilters[nbAdditionalContent] = document.createElement("ul");
                    additionalFilters.className = "list-unstyled";
                    additionalFilters[nbAdditionalContent].style.marginTop = "-10px";
                    voirPlus[nbExpandable] = document.createElement("text");
                    voirPlus[nbExpandable].id = nbExpandable + "filter";
                    voirPlus[nbExpandable].innerHTML = "voir plus +";
                    voirPlus[nbExpandable].className = "collapsible";
                    voirPlus[nbExpandable].style = "font-weight: bold";
                    voirPlus[nbExpandable].addEventListener("click", function() {
                        (showAdditonalContent(document.getElementById(this.id), voirPlus, additionalFilters, bool))
                    });
                    voirPlus[nbExpandable + 1] = document.createElement("div");
                    voirPlus[nbExpandable + 1].className = "content";
                    while (listSearchFilters.children[i].children[1].childNodes[y] != null &&
                        listSearchFilters.children[i].children[1].childNodes[y].nodeName == "LI") {
                        additionalFilters[nbAdditionalContent].appendChild(listSearchFilters.children[i].children[1].childNodes[y]);
                        additionalFilters[nbAdditionalContent].style.marginLeft = "-16px";
                        y++;
                    }
                    listSearchFilters.children[i].children[1].appendChild(voirPlus[nbExpandable]);
                    listSearchFilters.children[i].children[1].appendChild(voirPlus[nbExpandable + 1]);
                    nbAdditionalContent++;
                    nbExpandable = nbExpandable + 2;
                    break;
                }
            }
        }
    }
    while (h2Description = document.querySelector(".category-head-content .category_description h2")) {
        if (h2Description) {
            let SEO = document.createElement('div')
            SEO.className = "SEO"
            h2Description.style = "font-size : 26px"
            SEO.appendChild(h2Description.nextElementSibling)
            SEO.insertBefore(h2Description, SEO.lastElementChild)
            let mainContent = document.querySelector(".main-content")
            mainContent.appendChild(SEO)
            SEO.style = "width : 60% ; margin : auto; margin-left : 25%"
        }
    }
    while (h2Description = document.querySelector(".all-listings-head-content .all_listings_description h2")) {
        if (h2Description) {
            let SEO = document.createElement('div')
            SEO.className = "SEO"
            h2Description.style = "font-size : 26px"
            SEO.appendChild(h2Description.nextElementSibling)
            SEO.insertBefore(h2Description, SEO.lastElementChild)
            let mainContent = document.querySelector(".main-content")
            mainContent.appendChild(SEO)
            SEO.style = "width : 60%; margin : auto; margin-left : 25%"
        }
    }
}

if (listing && !(window.location.pathname == "/listing/cy6l4f82jt8y-test" || window.location.pathname == "/listing/2h10s7y3qi-test" ||
        window.location.pathname == "/listing/bz4bzu8sduz-carte-cadeau" || window.location.pathname == "/listing/cba3a5wvy4r-carte-cadeau" ||
        window.location.pathname == "/listing/8ml52rd8bg-carte-cadeau")) {

    var place = document.querySelector('.listing_description .container .listing_detail_content')
    var place2 = document.querySelector('.listing_description .container')

    var rate = document.querySelector('.rate')
    if (listing.supplier.note == 0) {
        rate.style = "display : none"
    }

    var gift = document.createElement('input')
    gift.setAttribute("type", "hidden")
    gift.setAttribute("name", "lGift")
    gift.setAttribute("value", "0")
    gift.setAttribute("id", "isAgift")
    if (document.querySelector(".add-to-cart-panel form")) {
        document.querySelector(".add-to-cart-panel form").insertBefore(gift, document.querySelector(".add-to-cart-panel form").lastElementChild)
    }

    var addFav = document.querySelector('.add-to-favorite-wrapper')
    var Offer = document.createElement('a')
    Offer.setAttribute("id", "make-gift")
    Offer.className = "add-to-favorite btn"
    Offer.setAttribute("onClick", "makeGift()")
    Offer.href = "#"
    var giftIcon = document.createElement('i')
    giftIcon.className = "fa fa-gift"
    Offer.appendChild(giftIcon)
    var offerTxt = document.createElement('small')
    offerTxt.style = "padding-left : 6px"
    offerTxt.appendChild(document.createTextNode("Offrir cette activité"))
    Offer.appendChild(offerTxt)
    addFav.appendChild(Offer)
    if (document.querySelector(".add-to-cart-panel form")) {
        document.querySelector(".add-to-cart-panel form").appendChild(addFav)
    }

    var listDetailContent = document.querySelector('.listing_description .container .listing_detail_content')

    var sectionDescription = document.createElement('div')
    sectionDescription.className = "col-md-8 section_desc"

    var generalDescription = document.createElement('div')
    var titleGeneralDescription = document.querySelector('.listing_description .container .listing_detail_content h2')
    var descGeneralDescription = document.querySelector('.listing_description .container .listing_detail_content .listing-description')
    if (descGeneralDescription) {
        descGeneralDescription.insertBefore(titleGeneralDescription, descGeneralDescription.firstElementChild)
    }
    //generalDescription.appendChild(titleGeneralDescription)
    sectionDescription.appendChild(generalDescription)
    place2.insertBefore(sectionDescription, place2.lastElementChild);



    var sideDescription = document.querySelector('.listing_details_list')
    var sideLocation = document.querySelector('.listing_details_list_location a')
    if (listing.attributes.lieu_proposes === "À distance") {
        document.querySelector('.listing_details_list_location').style = "display : none"
    } else if (listing.attributes.code_postal) {
        sideLocation.appendChild(document.createTextNode(listing.attributes.code_postal))
    }
    if (listing.attributes.duree_de_l_activite) {
        var duree = document.createElement('li')
        duree.className = "listing_details_list_duree bold"
        duree.appendChild(document.createTextNode(listing.attributes.duree_de_l_activite))
        sideDescription.insertBefore(duree, sideDescription.firstElementChild);
    }
    var lvl = document.createElement('li')
    lvl.className = "listing_details_list_lvl bold"
    var niveaux = listing.attributes.niveau_requis_2
    if (niveaux) {
        niveaux.forEach(function(element) {
            lvl.appendChild(document.createTextNode(element + (niveaux.indexOf(element) === niveaux.length - 1 ? "" : " / ")))
        });
    }
    sideDescription.insertBefore(lvl, sideDescription.firstElementChild)

    if ("/" + window.location.pathname.split('/')[1] === "/listing") {
        if (document.getElementsByClassName("breadcrumb-item")[1].innerText === "coffrets") {
            let divButtons = document.querySelector("div.add-to-favorite-wrapper");
            let purchaseButton = document.createElement("a");
            let lid = document.getElementsByName("lid");
            let div = document.getElementsByClassName("listing_details_list");

            div[0].children[1].textContent = "";
            div[0].children[2].textContent = "";
            purchaseButton.href += "https://neosilver.fr/checkout/summary?lGift=0&lid=" + lid[0].value;
            purchaseButton.className += "btn btn-rounded btn-primary add-to-cart";
            purchaseButton.textContent += "Acheter";
            divButtons.insertBefore(purchaseButton, divButtons.firstChild);
            let bookButton = document.querySelector(".submit .btn.btn-rounded.btn-primary.add-to-cart")
            if (bookButton) {
                bookButton.style = "display : none"
            }
        }
    }

    var sectionProf = document.createElement('div')
    sectionProf.className = "listing_detail_list_prof"

    sectionProf.style = "display : inline-block ; width : 100%"
    if (window.location.pathname === "/listing/cba3a5wvy4r-carte-cadeau-de-25-euros" || window.location.pathname === "/listing/bz4bzu8sduz-carte-cadeau-50-euros" ||
        window.location.pathname === "/listing/8ml52rd8bg-la-carte-cadeau-de-100-euros") {
        sectionProf.style = "display: none"
    }
    var titleProf = document.createElement('h2')
    titleProf.appendChild(document.createTextNode("À propos du responsable de l'activité"))
    var descProf = document.querySelector(".listing-attribut-description_professeur")
        //descProf.className = "col-md-8"

    sectionProf.appendChild(titleProf)
        //console.log(descProf)
    if (descProf) {
        sectionProf.appendChild(descProf)
    }


    generalDescription.insertBefore(sectionProf, generalDescription.firstElementChild);
    generalDescription.appendChild(listDetailContent)

    var sectionLeft = document.createElement('div')
    sectionLeft.className = "col-md-4"

    var PP = document.createElement('div')
    var photoProf = document.createElement('img')
    photoProf.className = "col-md-4"
    photoProf.src = listing.supplier.avatar
    photoProf.width = "100"
    photoProf.height = "100"
    photoProf.style = "min-width: 250px;margin: 0px auto 24px auto;display: block;width: auto;min-height: 250px;border-radius: 400px;padding: 0px; float: inherit;"
    PP.appendChild(photoProf)
    sectionLeft.appendChild(PP)


    var sectionIcone = document.createElement('div')
    sectionLeft.appendChild(sectionIcone)
    sectionLeft.style = "margin-bottom : 24px"
    sectionIcone.className = "section_icon" // here
    sectionIcone.style = "display : inline-block; padding : 16px 0; text-align : center"

    var col1 = document.createElement('div')
    col1.className = ""
    col1.style = "min-width : 250px ; margin-bottom : 32px ;margin-top : 16px ; padding: 0 48px;"
    var img1 = document.createElement('img')
    img1.style = "display: inline-block; margin-left: auto; margin-right: auto; background-color : none;"
    img1.src = "https://cdn.kreezalid.com/kreezalid/546837/files/987794/160x160_illu_en_ligne_recupere_14.png"
    img1.width = "100"
    img1.height = "100"
    var t1 = document.createElement('h4')
    t1.style = "text-align: center;"
    if (listing.attributes.niveau_requis_2) {
        listing.attributes.niveau_requis_2.forEach(function(element) {
            t1.appendChild(document.createTextNode(element + (niveaux.indexOf(element) === niveaux.length - 1 ? "" : " / ")))
        });
    }
    col1.appendChild(img1)
    col1.appendChild(t1)

    var col2 = document.createElement('div')
    col2.className = ""
    col2.style = "min-width : 250px ; margin-bottom : 32px ; margin-top : 16px;padding: 0 48px;"
    var img2 = document.createElement('img')
    img2.style = "display: inline-block; margin-left: auto; margin-right: auto;"
    img2.src = "https://cdn.kreezalid.com/kreezalid/546837/files/987794/160x160_illu_en_ligne_recupere_13.png"
    img2.width = "100"
    img2.height = "100"
    var t2 = document.createElement('h4')
    t2.style = "text-align: center;"
    t2.appendChild(document.createTextNode(listing.attributes.lieu_proposes === "À distance" ? listing.attributes.lieu_proposes : listing.location))
        //t2.appendChild(document.createTextNode(listing.location.slice(listing.location.search(' '), listing.location.length)))
    col2.appendChild(img2)
    col2.appendChild(t2)
    let descAddrSmall = document.createElement('small')
    let descAddrItalic = document.createElement('i')
    descAddrItalic.appendChild(document.createTextNode("Plus d'information après réservation"))
    descAddrSmall.appendChild(descAddrItalic)
    descAddrSmall.style = "display : block; margin-top : -10px; font-size : 60%"
        //col2.appendChild(descAddrSmall)


    var col3 = document.createElement('div')
    col3.className = ""
    col3.style = "min-width : 250px ; margin-bottom : 32px ;margin-top : 16px; padding: 0 48px;"
    var img3 = document.createElement('img')
    img3.style = "display: inline-block; margin-left: auto; margin-right: auto;"
    img3.src = "https://cdn.kreezalid.com/kreezalid/546837/files/987794/160x160_illu_en_ligne_recupere_12.png"
    img3.width = "100"
    img3.height = "100"
    var t3 = document.createElement('h4')
    t3.style = "text-align: center;"
    t3.appendChild(document.createTextNode("durée du cours : " + listing.attributes.duree_de_l_activite))
    col3.appendChild(img3)
    col3.appendChild(t3)

    var col4 = document.createElement('div')
    col4.className = ""
    col4.style = "min-width : 250px ; margin-bottom : 32px ; margin-top : 16px;padding: 0 48px;"
    var img4 = document.createElement('img')
    img4.style = "display: inline-block; margin-left: auto; margin-right: auto;"
    img4.src = "https://cdn.kreezalid.com/kreezalid/546837/files/987794/480x480_illu_en_ligne_recupere_11.png"
    img4.width = "100"
    img4.height = "100"
    var t4 = document.createElement('h4')
    t4.style = "text-align: center;"
    t4.appendChild(document.createTextNode("Nombre de participant(s) : " + listing.quantity))
    col4.appendChild(img4)
    col4.appendChild(t4)

    sectionIcone.appendChild(col1)
    sectionIcone.appendChild(col2)
    if (listing.attributes.duree_de_l_activite) {
        sectionIcone.appendChild(col3)
    }
    sectionIcone.appendChild(col4)

    place2.insertBefore(sectionLeft, place2.firstElementChild);

    if (listing.attributes.video) {
        var sectionVideo = document.querySelector('.listing-attribut-video')
        if (listing.attributes.video.search("vimeo") >= 0) {
            var video = document.createElement('iframe')
            video.src = "https://player.vimeo.com/video/" + ((listing.attributes.video.split("//"))[1].split("/"))[1] + "?embedparameter=value"
            video.width = "100%"
            video.height = "360"
            video.setAttribute("frameborder", "0")
            sectionVideo.appendChild(video)
        } else if (listing.attributes.video.search("youtube") >= 0) {
            var video = document.createElement('iframe')
            video.src = "https://www.youtube.com/embed/" + ((listing.attributes.video.split("="))[1])
            video.width = "100%"
            video.height = "360"
            video.id = "ytplayer"
            video.setAttribute("frameborder", "0")
            sectionVideo.appendChild(video)
        }
    }

    if (window.location.pathname === "/listing/cba3a5wvy4r-carte-cadeau-de-25-euros" || window.location.pathname === "/listing/bz4bzu8sduz-carte-cadeau-50-euros" ||
        window.location.pathname === "/listing/8ml52rd8bg-la-carte-cadeau-de-100-euros") {
        let purchaseButton = document.getElementById("make-gift");
        let div = document.querySelector("li.price");
        let discount = document.createElement("p");
        let message = document.createElement("p");

        purchaseButton.textContent = "Acheter cette carte cadeau";
        message.innerHTML = "<br /><br />-20% Réduction exceptionnelle !"
        message.style = "color: red; font-size: 18px;";
        document.querySelector(".listing_description .container .col-md-4").style = "display : none"
        document.querySelector(".listing_description .container .section_desc").setAttribute("class", "col-md-12 section_desc")

        if (window.location.pathname === "/listing/cba3a5wvy4r-carte-cadeau-de-25-euros") {
            discount.textContent = "25,00 €";
        } else if (window.location.pathname === "/listing/bz4bzu8sduz-carte-cadeau-50-euros") {
            discount.textContent = "50,00 €";
        } else if (window.location.pathname === "/listing/8ml52rd8bg-la-carte-cadeau-de-100-euros") {
            discount.textContent = "100,00 €";
        }
        discount.style = "color: black; font-size: 20px; text-decoration: line-through;";

        div.insertBefore(discount, div.firstChild);
        div.insertBefore(message, div.lastChild);
    }

    let divButtonToGoBuy = document.createElement('div')
    divButtonToGoBuy.style = "width : 100%; text-align : right; margin-bottom: 24px;"
    let ButtonToGoBuy = document.createElement('a')
    ButtonToGoBuy.setAttribute("class", "btn smoothscroll")
    ButtonToGoBuy.setAttribute("href", "#covidAlert")
    ButtonToGoBuy.appendChild(document.createTextNode("Réserver"))
    ButtonToGoBuy.style = "margin : 0 8px"
    let ButtonToGoQuestion = document.createElement('a')
    ButtonToGoQuestion.setAttribute("class", "btn smoothscroll")
    ButtonToGoQuestion.setAttribute("href", "/listing/contact?id=" + listing.id)
    ButtonToGoQuestion.style = "margin : 0 8px"
    ButtonToGoQuestion.appendChild(document.createTextNode("Poser une question"))
    divButtonToGoBuy.appendChild(ButtonToGoQuestion)
    divButtonToGoBuy.appendChild(ButtonToGoBuy)
    document.querySelector(".section_desc").insertBefore(divButtonToGoBuy, document.createTextNode(".section_desc").firstElementChild)

    if (listing.attributes.lieu_proposes == "À distance" && document.querySelector("#listing_localisation")) {
        document.querySelector("#listing_localisation").style = "display:none"

    }
    if (!document.querySelector('.price').firstElementChild) {
        let price = document.createElement('span')
        price.className = "price"
        let priceContent = document.createElement('span')
        priceContent.className = "price_content"
        priceContent.appendChild(document.createTextNode(listing.price_with_currency))
        price.appendChild(priceContent)
        document.querySelector('.price').appendChild(price)
    }
}

if (window.location.pathname.search("/checkout/summary") != -1) {
    let divButtons = document.querySelector("div.payment_opt");
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);
    let lid = urlParams.get("lid");
    let date_selected = urlParams.get("date_selected") || urlParams.get("startdate") + urlParams.get("starttime");;
    let qty_select = urlParams.get("qty_select");
    let lGift = urlParams.get("lGift");

    if (lGift === "1" && (lid === "38753" || lid === "38754" || lid === "38755")) {
        let bookingSummary = document.getElementsByClassName("booking_summary_box");
        let bookingComment = document.getElementsByClassName("row checkout");

        bookingSummary[0].children[3].children[0].innerHTML = "<strong>Je reçois un mail de confirmation de paiement.</strong><br /><br />";
        bookingSummary[0].children[3].children[1].innerHTML = "<strong>Je reçois la carte cadeau avec un code utilisable sur le site Neosilver.</strong><br /><br />";
        bookingSummary[0].children[3].children[2].innerHTML = "<strong>J'imprime ou je transfère le mail à mon bénéficiaire qui pourra utiliser son code sur le site Neosilver.</strong><br /><br />";
        bookingSummary[0].children[5].style = "display : none";
        bookingSummary[0].children[6].style = "display : none";
        bookingComment[0].children[0].innerHTML = "<strong>Ajoutez un texte personnalisé pour l'heureux élu<strong>";
        bookingComment[0].children[0].style = "font-size: 25px;";
    }
    if (Kr.AuthenticatedUser && lid && date_selected && qty_select && lGift) {
        let uid = Kr.AuthenticatedUser.id;
        let date = document.getElementById("booking-start-date-value").textContent.trim().replace(/[^a-z0-9]/gmi, ' ').replace(/\s+/g, '_');
        let giftCardButton = document.createElement("a");
        let giftCardButton2 = document.createElement("a");

        giftCardButton.href += "https://carteneosilver.herokuapp.com/gift_card?lid=" + lid + "&date_selected=" + date_selected + "&qty_select=" + qty_select + "&lGift=" + lGift + "&uid=" + uid + "&date=" + date + "&b=1";
        giftCardButton.className += "btn btn-primary";
        giftCardButton.style = "margin: 10px 0px";
        giftCardButton.appendChild(document.createTextNode("Utiliser une carte cadeau"));
        divButtons.insertBefore(giftCardButton, divButtons.lastChild);
        divButtons.insertBefore(document.createElement("br"), divButtons.lastChild);
        giftCardButton2.href += "https://carteneosilver.herokuapp.com/gift_card?lid=" + lid + "&date_selected=" + date_selected + "&qty_select=" + qty_select + "&lGift=" + lGift + "&uid=" + uid + "&date=" + date + "&b=2";
        giftCardButton2.className += "btn btn-primary";
        giftCardButton2.appendChild(document.createTextNode("Appliquer un code de réduction"));
        divButtons.insertBefore(giftCardButton2, divButtons.lastChild);
    } else if (!Kr.AuthenticatedUser) {
        let errSignIn = document.createElement("div");

        errSignIn.style = "display: inherit; padding-top: 32px; font-size: 15px; text-align: left; color: 2F4F4F";
        errSignIn.appendChild(document.createTextNode("Un compte est requis pour procéder au paiement."));
        divButtons.insertBefore(errSignIn, divButtons.lastChild);

    } else {
        let errUrlParams = document.createElement("div");

        errUrlParams.style = "display: inherit; padding-top: 32px; font-size: 15px; text-align: left; color: 2F4F4F";
        errUrlParams.appendChild(document.createTextNode("Cette annonce n'est pas éligible au paiement par carte cadeau."));
        divButtons.insertBefore(errUrlParams, divButtons.lastChild);
    }
}

if (window.location.pathname.search("/") != -1) {
    let homepage = document.querySelector("div.homepage-hero-module");
    let div = document.createElement("div");
    let title = document.createElement("h1");
    let subtitle = document.createElement("h2");

    div.id = "homepageDiv";
    title.id = "homepageTitle2";
    subtitle.id = "homepageSubtitle";
    subtitle.innerHTML = "Découvrez des dizaines d'activités<br />accessibles depuis chez vous.";
    title.innerHTML = "<span id='homepageTitle1'>ACTIVITES</span><br />A DISTANCE";

    div.insertBefore(subtitle, div.firstChild);
    div.insertBefore(title, div.firstChild);
    if (homepage) {
        homepage.insertBefore(div, homepage.lastChild);
    }
}

if (window.location.pathname.search("/calendar?") != -1) {

    fetchData();

    function fetchData() {
        id = window.location.search.split("=")[1];
        password = "d10573f30e74800ed7e93988fd82d42d6895791c9bfc6f1c50";
        username = "developpement@neosilver.fr";
        basicAuth = "Basic " + btoa(`${username}:${password}`);

        var myInit = {
            method: 'GET',
            headers: {
                'Authorization': basicAuth,
                'Accept': 'application/json',
            },
        };

        var request = new Request("https://neosilver.fr/api/v1/listings/" + id + ".json", myInit);

        fetch(request).then(function(response) {
                return response.json();
            })
            .then(function(body) {
                body.listing = setListing(body.listing);
                setCalendar(body.listing);
            });
    }

    function setListing(listing) {
        for (const [key, value] of Object.entries(listing.attributes)) {
            if (`${key}` == "lundi_debut") {
                return listing;
            }
        }
        for (let i = 0; i < 7; i++) {
            var key1 = jourSemaine[i] + "_debut";
            var key2 = jourSemaine[i] + "_fin";
            listing.attributes[key1] = "0";
            listing.attributes[key2] = "23";
        }
        return listing;
    }

    function setCalendar(listing) {
        listingAttributes = listing.attributes;
        divCalendar = document.createElement("div");
        divCalendar.id = "dateCalendar";
        divCalendar.style = "margin-top: 7%; text-align: center;";
        formCalendar = document.createElement("form");
        formCalendar.enctype = "multipart/form-data";
        formCalendar.method = "post";
        formCalendar.setAttribute('accept-charset', "utf-8");
        formCalendar.id = "listingForm";
        formCalendar.role = "form";
        formCalendar.action = "/listing/edit/" + listing.id;
        formCalendar.target = "frame";
        formCalendar.style = "border: 1px solid black; margin: auto; margin-bottom: 10%; width: 350px;";

        iframe = document.createElement("iframe");
        iframe.id = "frameCalendar";
        iframe.name = "frame";
        iframe.setAttribute("position", "absolute");
        iframe.setAttribute("width", "0");
        iframe.setAttribute("height", "0");
        iframe.setAttribute("border", "0");

        helper = document.createElement("p");
        helper.innerHTML = "* Les heures de fin correspondent aux heures pleines.";
        helper.style = "opacity: 70%";
        item = document.createElement("ul");

        var selectDebut = document.createElement("select");
        var selectFin = document.createElement("select");
        for (i = 0; i < 7; i++) {
            li = document.createElement("li");
            textInit = document.createElement("p");
            textInit.innerHTML = jourSemaine[i] + " de ";
            text = document.createElement("p");
            text.innerHTML = "à";
            star = document.createElement("p");
            star.innerHTML = "*";
            li.style = "display: flex; flex-direction: row; align-items: flex-end; justify-content: space-evenly; margin-bottom: 3%;";
            li.id = "listDays";
            selectDebut = feelSelects(jourSemaine[i])[0];
            selectFin = feelSelects(jourSemaine[i])[1];
            li.appendChild(textInit);
            li.appendChild(selectDebut);
            li.appendChild(text);
            li.appendChild(selectFin);
            li.appendChild(star);
            item.appendChild(li);
            valueDebut = parseInt(selectDebut.options[selectDebut.selectedIndex].text);
            valueFin = parseInt(selectFin.options[selectFin.selectedIndex].text);

            for (let x = 0; x < 24; x++)
                if (x >= parseInt(selectFin.options[selectFin.selectedIndex].text))
                    selectDebut.children[x].disabled = true;
            for (let x = 0; x < 24; x++)
                if (x <= parseInt(selectDebut.options[selectDebut.selectedIndex].text))
                    selectFin.children[x].disabled = true;
        }
        formCalendar.onsubmit = function() {
            for (let i = 0; i < 7; i++) {
                selectDebut = document.getElementById("attributes-" + jourSemaine[i] + "-debut");
                selectFin = document.getElementById("attributes-" + jourSemaine[i] + "-fin");

                if (parseInt(selectDebut.options[selectDebut.selectedIndex].text) >= parseInt(selectFin.options[selectFin.selectedIndex].text)) {
                    return false;
                }
            }
            return true;
        }
        item.style = "padding: 10% 10% 0%; margin: auto; display: flex; justify-content: flex-end; flex-wrap: wrap;";
        item.appendChild(helper);
        formCalendar.appendChild(item);
        let consigne = document.createElement("label");
        consigne.innerHTML = "Veuillez selectionner les plages horaires de votre activité.";
        consigne.style = "text-align: center;";
        divCalendar.appendChild(consigne);
        divCalendar.appendChild(formCalendar);
        document.getElementsByClassName("container")[6].appendChild(divCalendar);
        document.getElementsByClassName("container")[6].appendChild(iframe);
        setRegisterBtn();
    }

    function setRegisterBtn() {
        btnDiv = document.createElement("div");
        btnDiv.className = "row submit_listing text-center";
        btnDiv.style = "position: relative";

        btn = document.createElement("button");
        btn.className = "btn btn-primary  text-uppercase";
        btn.setAttribute("type", "submit");
        btn.style = "position: absolute; transform: translate(-50%, 50%)";
        btn.innerHTML = "Enregistrer";
        btn.id = "liveToastBtn";

        div = document.createElement("div");
        div.className = "toast";
        div.role = "alert";
        div.setAttribute("aria-live", "assertive");
        div.setAttribute("aria-atomic", "true");
        div.id = "liveToast";
        div.style = "position: fixed; bottom: 2%; right: 100px; z-index: 10; width: auto;";
        div.innerHTML = '<div class="toast-header"><img src="https://upload.wikimedia.org/wikipedia/commons/d/d9/F1_green_flag.svg" class="rounded mr-2" alt="..." style="width: 20px; height: 20px;"><strong class="mr-auto">Success</strong></div><div class="toast-body"> Hello, world! This is a toast message.</div>'
        document.getElementsByClassName("container")[6].appendChild(div);
        document.getElementById("liveToast").scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });

        btn.onclick = function() {
            var toastElList = [].slice.call(document.querySelectorAll('.toast'))
            var toastList = toastElList.map(function(toastEl) {
                return new bootstrap.Toast(toastEl);
            });
            setPopMsg();
            toastList[0]._config.delay = 8000;
            toastList.forEach(toast => toast.show());
        };
        btnDiv.appendChild(btn);
        document.getElementById("listingForm").appendChild(btnDiv);
    }

    function setPopMsg() {
        let error = false;
        let i = 0;

        for (; i < 7; i++) {
            selectDebut = document.getElementById("attributes-" + jourSemaine[i] + "-debut");
            selectFin = document.getElementById("attributes-" + jourSemaine[i] + "-fin");

            if (parseInt(selectDebut.options[selectDebut.selectedIndex].text) >= parseInt(selectFin.options[selectFin.selectedIndex].text)) {
                selectDebut.style = "border: 1px solid red; width: 70px;";
                selectFin.style = "border: 1px solid red; width: 70px;";
                error = true;
            }
        }
        document.getElementsByClassName("toast")[0].style = "opacity: 100; position: fixed; bottom: 2%; right: 100px; z-index: 10; width: auto;";
        document.getElementsByClassName("toast-header")[0].children[1].style = "margin-left: 10px;";

        if (error == false) {
            document.getElementsByClassName("toast-header")[0].children[0].src = "https://upload.wikimedia.org/wikipedia/commons/d/d9/F1_green_flag.svg";
            document.getElementsByClassName("toast-header")[0].children[1].innerHTML = "Succès";
            document.getElementsByClassName("toast-body")[0].innerHTML = "L'enregistrement de vos horaires a été validé.";
            document.getElementById("liveToastBtn").scrollIntoView({ behavior: 'smooth', block: 'end', inline: "nearest" });
        } else if (error == true) {
            document.getElementsByClassName("toast-header")[0].children[0].src = "https://image.freepik.com/free-photo/card-soft-template-paper-report_1258-167.jpg";
            document.getElementsByClassName("toast-header")[0].children[1].innerHTML = "Erreur";
            document.getElementsByClassName("toast-body")[0].innerHTML = "Vérifiez les valeurs de vos données enregistrées.";
            document.getElementById("liveToastBtn").scrollIntoView({ behavior: 'smooth', block: 'end', inline: "nearest" });
            return false;
        }
        return true;
    }

    function feelSelects(jour) {
        let i = 0;
        selectDebut = document.createElement("select");
        selectFin = document.createElement("select");

        for (; jour != jourSemaine[i]; i++);
        selectDebut.onchange = function() {
            getFinSelect = document.getElementById("attributes-" + this.id.split("-")[1] + "-fin");
            debutValue = this.options[this.selectedIndex].text;
            finValue = getFinSelect.options[getFinSelect.selectedIndex].text;

            if (parseInt(debutValue) >= parseInt(finValue)) {
                this.style = "border: 1px solid red; width: 70px";
                getFinSelect.style = "border: 1px solid red; width: 70px";
            } else {
                this.style = "border: 1px solid #CCC; width: 70px";
                getFinSelect.style = "border: 1px solid #CCC; width: 70px";
            }
            for (let x = 0; x < selectDebut.childElementCount; x++) {
                if (x <= parseInt(this.options[this.selectedIndex].text)) {
                    getFinSelect.children[x].disabled = true;
                } else {
                    getFinSelect.children[x].disabled = false;
                }
            }
        }
        selectFin.onchange = function() {
            getDebutSelect = document.getElementById("attributes-" + this.id.split("-")[1] + "-debut");
            finValue = this.options[this.selectedIndex].text;
            debutValue = getDebutSelect.options[getDebutSelect.selectedIndex].text;

            if (parseInt(debutValue) >= parseInt(finValue)) {
                this.style = "border: 1px solid red; width: 70px";
                getDebutSelect.style = "border: 1px solid red; width: 70px";
            } else {
                this.style = "border: 1px solid #CCC; width: 70px";
                getDebutSelect.style = "border: 1px solid #CCC; width: 70px";
            }
            for (let x = 0; x < this.childElementCount; x++) {
                if (x >= parseInt(this.options[this.selectedIndex].text)) {
                    getDebutSelect.children[x].disabled = true;
                } else {
                    getDebutSelect.children[x].disabled = false;
                }
            }
        }

        selectDebut.name = "attributes[" + jourSemaine[i] + "_debut]";
        selectDebut.setAttribute("data-alias", jourSemaine[i] + "_debut");
        selectDebut.id = "attributes-" + jourSemaine[i] + "-debut";
        selectDebut.className = "form-control";
        selectDebut.style = "width: 70px";

        selectFin.name = "attributes[" + jourSemaine[i] + "_fin]";
        selectFin.setAttribute("data-alias", jourSemaine[i] + "_fin");
        selectFin.id = "attributes-" + jourSemaine[i] + "-fin";
        selectFin.className = "form-control";
        selectFin.style = "width: 70px";

        let value = 209979 + (24 * 2) * i;
        if (i >= 2) { value = value - 1 }
        if (i >= 4) { value = value + 24 }

        for (x = 0; x < 24; x++, value++) {
            option = document.createElement("option");
            if (value == 210073) {
                value = 210344;
            }
            option.value = value;
            option.innerText = x.toString();
            selectDebut.appendChild(option)
        }

        for (x = 0; x < 24; x++, value++) {
            option = document.createElement("option");
            option.value = value;
            option.innerText = x.toString();
            selectFin.appendChild(option)
            if (value == 210073) {
                value = 210344;
            }
        }
        for (const [key, value] of Object.entries(listingAttributes)) {
            if (`${key}`.split("_")[0] == jourSemaine[i]) {
                if (`${key}`.split("_")[1] == "debut") {
                    selectDebut.children[parseInt(`${value}`)].setAttribute("selected", "selected");
                } else if (`${key}`.split("_")[1] == "fin") {
                    selectFin.children[parseInt(`${value}`)].setAttribute("selected", "selected");
                }
            }
        }
        return [selectDebut, selectFin];
    }

}

if (window.location.pathname.search("/listing/") != -1 && (window.location.pathname.search("/new") == -1) &&
    (window.location.pathname.search("/calendar") == -1) && (window.location.pathname.search("/edit") == -1)) {
    setHoraire();

    function setListing(listing) {
        for (const [key, value] of Object.entries(listing.attributes)) {
            if (`${key}` == "lundi_debut") {
                return;
            }
        }
        for (let i = 0; i < 7; i++) {
            var key1 = jourSemaine[i] + "_debut";
            var key2 = jourSemaine[i] + "_fin";
            listing.attributes[key1] = "0";
            listing.attributes[key2] = "23";
        }
    }


    function setHoraire() {
        var valueDebut = [];
        var valueFin = [];
        var allDays = ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"];
        var allMonths = ["January", "Febrary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var index;

        setListing(listing);
        if (document.getElementsByClassName("listing-attribut-" + jourSemaine[0] + "_debut")[0]) {
            for (i = 0; i < 7; i++) {
                valueDebut.push(document.getElementsByClassName("listing-attribut-" + jourSemaine[i] + "_debut")[0].children[0].children[1].children[0].innerHTML);
                valueFin.push(document.getElementsByClassName("listing-attribut-" + jourSemaine[i] + "_fin")[0].children[0].children[1].children[0].innerHTML)
            }
        } else {
            for (let i = 0; i < 7; i++) {
                valueDebut.push("0");
                valueFin.push("23");
            }
        }
        var today = new Date();
        var day = allDays[today.getDay()];
        var date = today.getFullYear() + '-' + "0" + (today.getMonth() + 1) + '-' + "0" + today.getDate();
        if (today.getDay() == 0) {
            index = 6;
        } else {
            index = today.getDay() - 1;
        }

        setTimetable(valueDebut[index], valueFin[index], day);
        document.getElementsByName("startdate")[0].onfocusout = function() {
            var month;
            var date;

            splitDate = (document.getElementsByName("startdate")[0].value).split("/");
            if (splitDate[1][0] != '0') {
                month = allMonths[splitDate[1]];
            } else {
                month = allMonths[parseInt(splitDate[1][1]) - 1];
            }
            date = new Date(month + " " + splitDate[0] + "," + splitDate[2]);
            var day = allDays[date.getDay()];
            var index;
            if (date.getDay() == 0) {
                index = 6;
            } else {
                index = date.getDay() - 1;
            }

            setTimetable(valueDebut[index], valueFin[index], day);
        }
    }

    function setTimetable(valueDebut, valueFin, day) {
        var selectHoraire = document.getElementById("StartTimeInput");
        selectHoraire.options.length = 0;

        if (valueDebut == "0 ")
            valueDebut = "0";
        for (let i = parseInt(valueDebut); i < parseInt(valueFin) + 1; i++, valueDebut++) {
            if (i < 10) {
                foption = document.createElement("option");
                foption.value = "0" + valueDebut + ":" + "00";
                foption.innerHTML = "0" + valueDebut + ":" + "00";

                soption = document.createElement("option");
                soption.value = "0" + valueDebut + ":" + "30";
                soption.innerHTML = "0" + valueDebut + ":" + "30";
            } else {
                foption = document.createElement("option");
                foption.value = valueDebut + ":" + "00";
                foption.innerHTML = valueDebut + ":" + "00";

                soption = document.createElement("option");
                soption.value = valueDebut + ":" + "30";
                soption.innerHTML = valueDebut + ":" + "30";

            }
            selectHoraire.appendChild(foption);
            selectHoraire.appendChild(soption);
        }
        if (valueFin < 10) {
            soption = document.createElement("option");
            soption.value = "0" + valueFin + ":" + "00";
            soption.innerHTML = "0" + valueFin + ":" + "00";
        } else {
            soption = document.createElement("option");
            soption.value = valueFin + ":" + "00";
            soption.innerHTML = valueFin + ":" + "00";
        }
    }

}

// nouveau fonctionnement de la page de creation, rien n'est load à l'initialisation

if (window.location.pathname.search("/listing/new") != -1 || window.location.pathname.search("/listing/edit") != -1) {
    if (document.querySelector("#category-id"))
        document.querySelector("#category-id").setAttribute("onchange", "categorySelected()")
}

function _categorySelected() {
    if (document.querySelector('#order-type-id')) {
        document.querySelector("#order-type-id").setAttribute("onchange", "categorySelected()")
    }
    let typeOffre = document.querySelector('#attributes-type-d-offre')
    if (typeOffre) {
        typeOffre.setAttribute("onchange", "setTypeOffre()")
    }
    if (document.querySelector('#nombre_d_activitee-input-div')) {
        document.querySelector('#nombre_d_activitee-input-div').parentElement.style = "display : none !important"
    }
    if (document.querySelector('#duree_de_l_abonnement-input-div')) {
        document.querySelector('#duree_de_l_abonnement-input-div').parentElement.style = "display : none !important"
    }
    if (document.querySelector('#duree_du_stage-input-div')) {
        document.querySelector('#duree_du_stage-input-div').parentElement.style = "display : none !important"
    }
    let lieuProposesSelect = document.querySelector("#attributes-lieu-proposes")
    if (lieuProposesSelect) {
        lieuProposesSelect.setAttribute("onchange", "setLieuProposes()")
    }
    if (document.querySelector("#conf_link-input-div")) {
        document.querySelector("#conf_link-input-div").parentNode.style = "display : none"
    }
    setLieuProposes()
}

function categorySelected() {
    window.setTimeout(_categorySelected, 500)
}

if (window.location.pathname.search("/listing/new") != -1 || window.location.pathname.search("/listing/edit") != -1) {
    if (window.location.pathname.search("/listing/edit") != -1) {
        window.setTimeout(_categorySelected, 500)
    }
    let exclu = document.querySelector("#exclu_thibault-input-div")
    if (exclu) {
        exclu.parentNode.style = "display : none"
    }
    if (document.querySelector('.add-new-date-btn')) {
        let addNewDate = document.querySelector('.add-new-date-btn')
        addNewDate.setAttribute("onclick", "addNewDate()")
        addNewDate.addEventListener('click', addNewDate2)
    }


    let formForm = document.querySelector("#listingForm");
    let cpForm = document.querySelector('#attributes-code-postal')

    if (cpForm) {
        cpForm.parentNode.parentNode.style = "display: none;"
        cpForm.parentNode.parentNode.className = "code-postal"
    }

    let typeOffre = document.querySelector('#attributes-type-d-offre')
    if (typeOffre) {
        typeOffre.setAttribute("onchange", "setTypeOffre()")
    }

    // … pour prendre en charge l'événement soumission
    if (formForm && window.location.pathname !== "/listing/edit/cba3a5wvy4r" && window.location.pathname !== "/listing/edit/bz4bzu8sduz" &&
        window.location.pathname !== "/listing/edit/8ml52rd8bg") {
        formForm.addEventListener('submit', function(event) {
            event.preventDefault();
            if (document.querySelector('.event_dates_form')) {
                let dateActivities = document.querySelector('.event_dates_form').firstElementChild
                let matin = document.querySelector('#attributes-moment-de-la-journee-176710')
                matin.removeAttribute("checked", "")
                let midis = document.querySelector('#attributes-moment-de-la-journee-176711')
                midis.removeAttribute("checked", "")
                let apresMidis = document.querySelector('#attributes-moment-de-la-journee-176712')
                apresMidis.removeAttribute("checked", "")
                let soiree = document.querySelector('#attributes-moment-de-la-journee-176713')
                soiree.removeAttribute("checked", "")

                while (dateActivities) {
                    if (dateActivities.className == "row") {
                        let hour = parseInt(dateActivities.children[0].children[0].children[0].children[3].children[0].value)
                        if (hour >= 8 && hour <= 11) {
                            matin.setAttribute("checked", "")
                        } else if (hour >= 12 && hour <= 13) {
                            midis.setAttribute("checked", "")

                        } else if (hour >= 14 && hour <= 17) {
                            apresMidis.setAttribute("checked", "")

                        } else if (hour >= 18 && hour <= 22) {
                            soiree.setAttribute("checked", "")
                        }
                    }
                    dateActivities = dateActivities.nextElementSibling;
                }
            }
            document.getElementById("listingForm").submit()
        });
    }
    if (document.querySelector('#attributes-nombre-d-activitee')) {
        let nbActivities = document.querySelector('#attributes-nombre-d-activitee').parentNode.parentNode
        nbActivities.style = "display:none"
    }
    if (document.querySelector('#attributes-duree-de-l-abonnement')) {
        let timeSub = document.querySelector('#attributes-duree-de-l-abonnement').parentNode.parentNode
        if (timeSub) {
            timeSub.style = "display:none"
        }
    }
    if (document.querySelector('#attributes-duree-du-stage')) {
        document.querySelector('#attributes-duree-du-stage').parentNode.parentNode.style = "display : none"
    }

    if (document.querySelector("#conf_link-input-div")) {
        document.querySelector("#conf_link-input-div").parentNode.style = "display : none"
    }
}

function setTypeOffre() {
    let values = document.querySelector('#attributes-type-d-offre').value
    if (values == 176633) {
        document.querySelector('#attributes-nombre-d-activitee').parentNode.parentNode.style = "display : inherit"
        document.querySelector('#attributes-duree-de-l-abonnement').parentNode.parentNode.style = "display : none"
        document.querySelector('#attributes-duree-du-stage').parentNode.parentNode.style = "display : none"

    } else if (values == 176634) {
        document.querySelector('#attributes-duree-de-l-abonnement').parentNode.parentNode.style = "display : inherit"
        document.querySelector('#attributes-nombre-d-activitee').parentNode.parentNode.style = "display : none"
        document.querySelector('#attributes-duree-du-stage').parentNode.parentNode.style = "display : none"
    } else if (values == 176632) {
        document.querySelector('#attributes-nombre-d-activitee').parentNode.parentNode.style = "display : none"
        document.querySelector('#attributes-duree-de-l-abonnement').parentNode.parentNode.style = "display : none"
        document.querySelector('#attributes-duree-du-stage').parentNode.parentNode.style = "display : none"
    } else {
        document.querySelector('#attributes-nombre-d-activitee').parentNode.parentNode.style = "display : none"
        document.querySelector('#attributes-duree-de-l-abonnement').parentNode.parentNode.style = "display : none"
        document.querySelector('#attributes-duree-du-stage').parentNode.parentNode.style = "display : inherit"
    }

}

function setLieuProposes() {
    let value = document.querySelector('#attributes-lieu-proposes')
    let addressCity = document.querySelector('#address-city');
    let addressCountry = document.querySelector('#address-country');
    let location = document.querySelector('#location');
    let mapLocation = document.querySelector('#listing_map_display');
    let codaPostal = document.querySelector('#attributes-code-postal')
    let confLink = document.querySelector("#conf_link-input-div")
    if (value && value.value == 191645) {
        if (addressCity) {
            addressCity.removeAttribute("required");
            addressCity.parentNode.parentNode.parentNode.style = "display : none";
            addressCity.value = null
        }
        if (addressCountry) {
            addressCountry.removeAttribute("required");
            addressCountry.value = null
        }
        if (location) {
            location.removeAttribute("required");
            location.value = null
            location.parentNode.parentNode.parentNode.style = "display : none";
            location.parentNode.parentNode.parentNode.previousElementSibling.style = "display : none";
        }
        if (mapLocation) { mapLocation.style = "display : none" }
        if (codaPostal) {
            codaPostal.value = null
            codaPostal.parentNode.parentNode.style = "display : none"
        }
        if (confLink) { confLink.parentNode.style = "display : block !important" }
    } else {
        if (addressCountry) {
            addressCountry.setAttribute("required", "");
        }
        if (addressCity) {
            addressCity.setAttribute("required", "");
            addressCity.parentNode.parentNode.parentNode.style = "display : block"
        }
        if (location) {
            location.setAttribute("required", "");
            location.parentNode.parentNode.parentNode.previousElementSibling.style = "display : block";
            location.parentNode.parentNode.parentNode.style = "display : block";
        }
        if (mapLocation) { mapLocation.style = "display : block; position: relative; overflow: hidden;" }
        if (codaPostal) { codaPostal.parentNode.parentNode.style = "display : block" }
        if (confLink) { confLink.parentNode.style = "display : none" }
    }
}

function addNewDate() {
    let eventDateForm = document.querySelector('.event_dates_form')
}

function addNewDate2() {
    let eventDateForm = document.querySelector('.event_dates_form')
}

var msg = document.querySelector('.contact_form_supplier_form h2')
if (msg) {
    msg.innerHTML = "Envoyer un message à Neosilver"
}
// var sectionDate = document.querySelector('.datetime .list-inline')
// if (sectionDate) {
//  var sectionDateYear = document.querySelector('.year')
//  sectionDate.appendChild(sectionDateYear)
// }


var menu = document.querySelector('#cbp-hrmenu > ul');
menu.removeChild(menu.firstChild);

var form = document.getElementById("formCarteCadeau");

// … pour prendre en charge l'événement soumission
if (form) {
    form.addEventListener('submit', function(event) {
        event.preventDefault();
        document.getElementById("message").value += " - " + document.getElementById("nom").value + " - " + document.getElementById("prenom").value +
            " - " + document.getElementById("Numéro").value + " - " + document.getElementById("date").value + " - " + document.getElementById("heure").value;
        document.getElementById("formCarteCadeau").submit()
    });
}

(function(m, a, i, l, e, r) {
    m['MailerLiteObject'] = e;

    function f() {
        var c = { a: arguments, q: [] };
        var r = this.push(c);
        return "number" != typeof r ? r : f.bind(c.q);
    }
    f.q = f.q || [];
    m[e] = m[e] || f.bind(f.q);
    m[e].q = m[e].q || f.q;
    r = a.createElement(i);
    var _ = a.getElementsByTagName(i)[0];
    r.async = 1;
    r.src = l + '?v' + (~~(new Date().getTime() / 1000000));
    _.parentNode.insertBefore(r, _);
})(window, document, 'script', 'https://static.mailerlite.com/js/universal.js', 'ml');

var ml_account = ml('accounts', '1670354', 'v5m6p3u4l5', 'load');

if (window.location.pathname == "/") {
    document.querySelector(".home_featured_listings.section-spacing .container").style = "padding-bottom : 2.5em ; border-top : none !important ; border-bottom : solid 1px rgb(111,111,111)"
}

let navElements = document.getElementsByClassName("univers-link");

if (!isMobileDevice()) {
    for (let i = 0; i < navElements.length; i++) {
        if (navElements[i].className === "univers-link has-subcategories") {
            navElements[i].onmouseenter = function() {
                if (navElements[i].nextElementSibling) {
                    navElements[i].nextElementSibling.style = "display: block;";
                }
                for (let j = 0; j < navElements.length; j++) {
                    if (j !== i && navElements[j].nextElementSibling) {
                        navElements[j].nextElementSibling.style = "display: none;";
                    }
                }
            }
            navElements[i].nextElementSibling.onmouseleave = function() {
                navElements[i].nextElementSibling.style = "display: none;";
            }
        } else {
            navElements[i].onmouseenter = function() {
                for (let j = 0; j < navElements.length; j++) {
                    if (j !== i && navElements[j].nextElementSibling) {
                        navElements[j].nextElementSibling.style = "display: none;";
                    }
                }
            }
        }
    }

    navElements[0].onclick = function() {
        window.location.href = "https://neosilver.fr/listing/category/sport-bien-etre-a-distance";
    }
    navElements[1].onclick = function() {
        window.location.href = "https://neosilver.fr/listing/category/conferences-a-distance";
    }
    navElements[2].onclick = function() {
        window.location.href = "https://neosilver.fr/listing/category/art-a-distance";
    }
    navElements[3].onclick = function() {
        window.location.href = "https://neosilver.fr/listing/category/les-coffrets";
    }
    navElements[4].onclick = function() {
        window.location.href = "https://neosilver.fr/listing/category/activites-en-presentiel";
    }
}

let covidPart = document.createElement("div")
covidPart.setAttribute('class', 'covidAlert')
covidPart.setAttribute('id', 'covidAlert')
covidPart.appendChild(document.createElement("p"))
covidPart.firstElementChild.appendChild(document.createTextNode("Réservez en toute confiance avec notre "))
let covidGaranti = document.createElement("a")
covidGaranti.setAttribute("href", "https://neosilver.fr/comment-ca-marche")
let covidGarantiText = document.createElement("strong")
covidGaranti.style = "text-decoration : underline"
covidGarantiText.appendChild(document.createTextNode("garantie  COVID "))
covidGaranti.appendChild(covidGarantiText)
covidPart.firstElementChild.appendChild(covidGaranti)
covidPart.firstElementChild.appendChild(document.createTextNode("OU réservez une "))
let covidActivity = document.createElement("a")
covidActivity.setAttribute("href", "https://neosilver.fr/listing/search?lieu_proposes%5B%5D=191645")
let covidActivityText = document.createElement("strong")
covidActivityText.appendChild(document.createTextNode("activité à distance "))
covidActivityText.style = "text-decoration : underline"
covidActivity.appendChild(covidActivityText)
covidPart.firstElementChild.appendChild(covidActivity)
covidPart.firstElementChild.appendChild(document.createTextNode("et en sécurité depuis votre domicile"))
covidPart.style = "text-align : center; padding: 5px; background-color : #EEF6F6;"
covidPart.firstElementChild.style = "color : #315455 !important; margin : 3px"
document.querySelector('.main-content').insertBefore(covidPart, document.querySelector('.main-content').firstElementChild)

if (window.location.pathname.search("/cartes-cadeaux") != -1) {
    console.log("tmtc");
    console.log(document.getElementsByClassName("cms-page-content"));
    document.getElementsByClassName('container static-page')[0].setAttribute("class", "static-page");
    document.getElementsByClassName('cms-page-content')[0].setAttribute("class", "");
    console.log(document.getElementsByClassName('page-header cms-page')[0])
    document.getElementsByClassName('page-header cms-page')[0].style.display = "none";
}